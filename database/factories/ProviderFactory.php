<?php

use Faker\Generator as Faker;

$factory->define(App\Provider::class, function (Faker $faker) {
    return [
        'document' => $faker->randomNumber, 
        'name' => $faker->name, 
        'representative' => $faker->name, 
        'email' => $faker->email, 
        'mobil' => '5785587456', 
        'phone' => '57884585', 
        'address' => $faker->address
    ];
});
