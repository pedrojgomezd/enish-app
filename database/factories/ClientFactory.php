<?php

use Faker\Generator as Faker;

$factory->define(App\Client::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'mobil' => '58987456216',
        'address' => $faker->address,
        'price_id' => 1
    ];
});
