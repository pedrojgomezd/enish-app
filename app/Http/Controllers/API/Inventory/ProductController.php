<?php

namespace App\Http\Controllers\API\Inventory;

use App\Http\Controllers\Controller;
use App\Models\Galery\Image;
use App\Models\Inventory\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        return ['data' => $product->orderBy('id', 'DESC')->get()->load('prices', 'articles', 'provider', 'imgs')];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'provider_id' => 'required',
            'name' => 'required',
            'description' => 'required',
            'bar_code' => 'required',
            'reference' => 'required',
            'initial_amount' => 'required',
            'cost' => 'required',
            'prices' => 'required',
            'articles' => 'required'
        ]);

        $product = Product::create($request->all());

        $product->articles()->createMany(
                $request->articles
            );

        $product->prices()->attach($request->prices);

        return $this->uploadFiles($request, $product);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Inventory\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return ['data' => $product->load('prices', 'articles', 'provider', 'imgs')];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Inventory\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Inventory\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    /**
     * UploadFiles
     * @param  Request $request 
     * @return collection
     */
    protected function uploadFiles(Request $request, $product)
    {

        if ($request->hasFile('file')) {        
        
            $now = now()->format('Y-m-d');
        
            foreach ($request->file('file') as $file) {
                $name_file = $file->storeAs("public/images/inventories/{$now}", "{$now}-{$file->getClientOriginalName()}");
                
                $product->imgs()->save(new Image(['url' => str_after($name_file, 'public')]));
            }

        }

        return $product->load('imgs');
    }
}
