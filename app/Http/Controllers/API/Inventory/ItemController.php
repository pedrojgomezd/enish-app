<?php

namespace App\Http\Controllers\API\Inventory;

use App\Http\Controllers\Controller;
use App\Http\Resources\Inventory\ItemsResource;
use App\Models\Inventory\InventoryItem;
use App\Provider;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        $items = InventoryItem::all()->load('photos', 'provider', 'invoices');
        
        return $items;
        //return ItemsResource::collection($items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        return $request->all();
        $request->validate(['name' => 'required',
                            'reference' => 'required',
                            'description' => 'required',
                            'provider' => 'required',
                            'file' => 'required']);

        $provider = Provider::find($request->provider);

        $item = $provider->inventory_items()->create($request->all());

        return $this->uploadFiles($request, $item);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(InventoryItem $item)
    {
        return $item->load('photos', 'provider', 'invoices');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    /**
     * UploadFiles
     * @param  Request $request 
     * @return collection
     */
    protected function uploadFiles(Request $request, $item)
    {

        if ($request->hasFile('file')) {        
        
            $now = now()->format('Y-m-d');            
        
            foreach ($request->file('file') as $file) {
                $name_file = $file->storeAs("public/inventories/{$now}", "{$now}-{$file->getClientOriginalName()}");
                
                $item->photos()->create(['url' => str_after($name_file, 'public'), 
                                        'ruta' => str_after($name_file, 'public'), 
                                        'description' => 'Sin description']);
            }

        }

        return $item->load('photos');
    }
}
