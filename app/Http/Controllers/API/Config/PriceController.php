<?php

namespace App\Http\Controllers\API\Config;

use App\Models\Config\Price;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Price::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required'
        ]);

        $price = Price::create($request->all());

        return ['data' => $price];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Config\Price  $price
     * @return \Illuminate\Http\Response
     */
    public function show(Price $price)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Config\Price  $price
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Price $price)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required'
        ]);

        $price->update($request->all());

        return ['data' => $price];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Config\Price  $price
     * @return \Illuminate\Http\Response
     */
    public function destroy(Price $price)
    {
    }
}
