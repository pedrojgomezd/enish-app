<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = ['size', 'color', 'quantity', 'quantity_sold'];

    public function product()
    {
    	return $this->belongsTo(Product::class);
    }
}
