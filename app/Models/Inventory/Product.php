<?php

namespace App\Models\Inventory;

use App\Models\Config\Price;
use App\Provider;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'bar_code', 'description', 'reference', 'initial_amount', 'quantity_sold', 'cost', 'provider_id'];

    public function articles()
    {
    	return $this->hasMany(Article::class);
    }

    public function prices()
    {
    	return $this->belongsToMany(Price::class)
    				->withPivot('amount');
    }

    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }

    /**
     * Get all of the tags for the post.
     */
    public function imgs()
    {
        return $this->morphToMany(\App\Models\Galery\Image::class, 'imggable');
    }
}
