<?php

namespace App\Models\Config;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $fillable = ['name', 'description'];
}
