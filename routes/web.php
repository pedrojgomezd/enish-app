<?php

use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function() {
    echo "site en construccion desea ingresar a <a href='/admin/dashboard'>Administrador</a> ?";
});*/

Auth::routes();

Route::get('/images', function() {
    $url = request()->url;
    return response(Storage::get('public/'.$url))->header('Content-type','image/png');
});

Route::any('/{any}', function() {
    return view('layouts.template');
})->where('any', '.*')->middleware('auth');

