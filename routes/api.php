<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('users', 'UserController');
Route::resource('clients', 'ClientController');
Route::resource('providers', 'ProviderController');
Route::group(['prefix' => 'inventories', 'namespace' => 'Inventory'], function() {
	Route::resource('items', 'ItemController');
	Route::resource('products', 'ProductController');
});

Route::group(['prefix' => 'invoice'], function() {
	Route::resource('sales', 'Invoice\SalesInvoiceController');    
});

Route::group(['prefix' => 'config', 'namespace' => 'Config'], function() {
    Route::resource('prices', 'PriceController');
});