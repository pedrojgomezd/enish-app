import Dashboard from './../pages/Dashboard'

const routes = [
	{
		path: '/dashboard',
		name: 'DASHBOARD',
		icon: 'si si-speedometer',
		component: Dashboard
	},
	{
		path: '/invoices',
		name: 'INGRESOS',
		icon: 'fa fa-hand-holding-usd',
		component: require('./../pages/invoices/InvoiceIndexPage'),
		children: [
			{
				path: '/invoices/',
				name: 'Facturas de venta',
				component: require('./../pages/invoices/InvoiceListPage')
			},
			{
				path: '/invoices/create',
				no_show: true,
				name: 'Facturas de venta',
				component: require('./../pages/invoices/InvoiceCreatePage')
			},
			{
				path: '/invoices/sale/:invoice',
				name: 'Facturas de venta',
				no_show: true,
				component: require('./../pages/invoices/InvoiceDetailsPage')
			},
		]
	},
	{
		path: '/clients/',
		name: 'CONTACTOS',
		icon: 'fa fa-address-book',
		component: require('./../pages/contacts/ContactIndexPage'),
		children: [
			{
				path: "/clients",
				name: 'Clientes',
				component: require('./../pages/contacts/clients/ListClientPage')
			},
			{
				path: "/clients/create",
				name: 'Crear',
				no_show: true,
				component: require('./../pages/contacts/clients/CreatePage')
			},
			{
				path: '/clients/:client',
				name: 'Edit',
				no_show: true,
				component: require('./../pages/contacts/clients/ClientShowPage')
			},
			{
				path: '/clients/:client/edit',
				name: 'Edit',
				no_show: true,
				component: require('./../pages/contacts/clients/ClientEditPage')
			},
			{
				path: "/providers",
				name: 'Proveedores',
				component: require('./../pages/contacts/providers/ProviderListPage')
			},
			{
				path: "/providers/create",
				name: 'Crear',
				no_show: true,
				component: require('./../pages/contacts/providers/ProviderCreatePage')
			},
			{
				path: '/providers/:provider',
				name: 'Edit',
				no_show: true,
				component: require('./../pages/contacts/providers/ProviderEditPage')
			}
		]
	},
	{
		path: '/inventories/',
		name: 'INVENTARIO',
		icon: 'fa fa-barcode',
		component: require('./../pages/inventory/InventoryIndexPage'),
		children: [
			{
				path: '/inventories',
				name: 'Lista de articulos',
				component: require('./../pages/inventory/products/ProductListPage'),	
			},
			{
				path: '/inventories/create',
				name: 'Agregar Item',
				component: require('./../pages/inventory/products/ProductCreatePage'),
			},
			{
					path: '/inventories/items/:item',
					name: 'Detalles Item',
					no_show: true,
					component: require('./../pages/inventory/products/ProductShowPage'),
			}
		]
	},
	{
		path: '/acounts',
		name: 'BANCOS',
		icon: 'fa fa-building',
		component: require('./../pages/acounts/AcountIndexPage'),
		children: [
			{
				path: '/acounts',
				name: 'Ver bancos',
				component: require('./../pages/acounts/AcountListPage'),
			},
			{
				path: '/acounts/create',
				name: 'Banco',
				no_show: true,
				component: require('./../pages/acounts/AcountCreatePage'),
			}
		]
	},
	{
		path: '/users',
		name: 'USUARIOS',
		icon: 'fa fa-users',
		component: require('./../pages/users/UserIndexPage'),
		children: [
			{
				path: '/users',
				name: 'Lista',
				component: require('./../pages/users/UserListPage')
			},
			{
				path: '/users/create',
				name: 'Crear',
				component: require('./../pages/users/UserCreatePage')
			},
			{
				path: '/users/:user',
				name: 'Edit',
				no_show: true,
				component: require('./../pages/users/UserEditPage')
			}
		]
	},
	{
		path: '/config',
		name: 'CONFIGURACIONES',
		icon: 'fa fa-load',
		component: require('./../pages/config/ConfigIndexPage'),
		children: [
			{
				path: '/prices',
				name: 'Precios',
				component: require('./../pages/config/prices/ConfigPriceList.vue')
			}
		]
	}
]

export default routes
