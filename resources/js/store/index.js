import Vue from 'vue'
import Vuex from 'vuex'

import users from './users'
import clients from './clients'
import providers from './providers'
import inventories from './inventories'
import invoices from './invoices'
import userLogin from './userLogin'
import config from './config'

Vue.use(Vuex)

const store = new Vuex.Store ({
	modules: {
		users,
		clients,
		providers,
		inventories,
		invoices,
		config,
		userLogin
	},
	state: {
		count: 0,
	}
})

export default store