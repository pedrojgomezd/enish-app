export default {
	getClients: ({commit}) => {
		axios.get('/api/clients').then(({data}) => {
			commit('setClients', data.data)				
		})
	},

	setClient: ({commit}, client) => {
		return new Promise( (resolver, reject) => {
			axios.post('/api/clients', client).then(({data}) => {
				commit('setClient', data.data)
				resolver()
			}).catch( ({response}) => {
				reject(response.data)
			})
		})
	},

	editClient: (context, id) => {
		return new Promise( (resolver, reject) => {
			axios.get(`/api/clients/${id}`).then( ({data}) => {
				context.commit('editClient', data.data)
				resolver(data)
			}).catch( ({response}) => {
				reject(response.data)
			})			
		})
	},

	updateClient: ({commit}, client) => {
		return new Promise( (resolver, reject) => {
			axios.put(`/api/clients/${client.id}`, client).then(({data}) =>{
				//commit('updateUser', data.data)
				resolver()
			}).catch(({response}) => reject(response.data))
		})
	}
}