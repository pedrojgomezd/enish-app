export default {
	allClients: state => {
		return state.clients
	},

	showClient: (state, getters) => {
		return state.client
	}
}