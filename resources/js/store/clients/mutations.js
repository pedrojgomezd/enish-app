export default {
	setClients: (state, clients) => {
		state.clients = clients
		state.loaded = true
	},

	setClient: (state, client) => {
		state.clients.push(client)
	},

	editClient: (state, client) => {
		state.client = client
	}
}