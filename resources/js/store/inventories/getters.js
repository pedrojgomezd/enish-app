export default {
	allArticles: state => {
		return state.articles
	},

	showArticle: (state, getters) => {
		return state.article
	},

	allProducts: state => {
		return state.products
	},

	showProduct: (state, getters) => {
		return state.product
	},
}