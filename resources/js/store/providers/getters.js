export default {
	allProviders: state => {
		return state.providers
	},

	showProvider: (state, getters) => {
		return state.provider
	}
}