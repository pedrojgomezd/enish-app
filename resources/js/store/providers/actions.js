export default {
	getProviders: ({commit}) => {
		axios.get('/api/providers').then(({data}) => {
			commit('setProviders', data.data)				
		})
	},

	setProvider: ({commit}, user) => {
		return new Promise( (resolver, reject) => {
			axios.post('/api/providers', user).then(({data}) => {
				commit('setProvider', data.data)
				resolver()
			}).catch( ({response}) => {
				reject(response.data)
			})
		})
	},

	editProvider: (context, id) => {
		return new Promise( (resolver, reject) => {
			axios.get(`/api/providers/${id}`).then( ({data}) => {
				context.commit('editProvider', data.data)
				resolver(data)
			}).catch( ({response}) => {
				reject(response.data)
			})			
		})
	},

	updateProvider: ({commit}, provider) => {
		return new Promise( (resolver, reject) => {
			axios.put(`/api/providers/${provider.id}`, provider).then(({data}) =>{
				//commit('updateUser', data.data)
				resolver()
			}).catch(({response}) => reject(response.data))
		})
	}
}