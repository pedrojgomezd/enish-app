export default {
	setProviders: (state, providers) => {
		state.providers = providers
		state.loaded = true
	},

	setProvider: (state, provider) => {
		state.providers.push(provider)
	},

	editProvider: (state, provider) => {
		state.provider = provider
	}
}