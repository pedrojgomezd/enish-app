export default {
	allUsers: state => {
		return state.users
	},

	showUser: (state, getters) => {
		return state.user
	}
}