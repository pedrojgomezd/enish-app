export default {
	setSales: (state, sales) => {
		state.sales = sales
		state.loaded = true
	},

	/*setProvider: (state, provider) => {
		state.providers.push(provider)
	},*/

	showSale: (state, sale) => {
		state.sale = sale
	},
}